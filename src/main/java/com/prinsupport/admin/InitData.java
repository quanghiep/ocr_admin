package com.prinsupport.admin;

import com.prinsupport.admin.modal.User;
import com.prinsupport.admin.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class InitData implements ApplicationRunner {

    @Autowired
    UserRepository userRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if(!userRepository.existsByUsername("admin")) {
            User user = User.builder().username("admin").password("admin").role("ADMIN").build().setPasswordToEncode();
            userRepository.save(user);
        }
    }
}

package com.prinsupport.admin.controller;

import com.prinsupport.admin.modal.User;
import com.prinsupport.admin.repository.UserRepository;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.Location;
import org.flywaydb.core.api.configuration.ClassicConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;
import java.util.Collections;

@RestController
public class HomeController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private DataSource dataSource;

    @GetMapping("/list")
    public ResponseEntity<?> getlist(){
        return ResponseEntity.ok(userRepository.findAll());
    }

    @PostMapping(value = "/api/admin/tenant/create")
    public ResponseEntity<?> createTenant(@RequestParam("username") String username){
        User userdb = userRepository.findByUsername(username);

        if(userdb != null)
            return ResponseEntity.badRequest().body("User is already exists");

        User user = User.builder().username(username).password(username).role("USER").build().setPasswordToEncode();
        userRepository.save(user);
        ClassicConfiguration classicConfiguration = new ClassicConfiguration();
        classicConfiguration.setLocations(new Location("db/migration/tenants"));
        classicConfiguration.setDataSource(dataSource);
        classicConfiguration.setSchemas(username);
        Flyway flyway = new Flyway(classicConfiguration);
        flyway.migrate();
        return ResponseEntity.ok(user);
    }
}
